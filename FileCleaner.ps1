# try {
#import mysql connector
add-type -Assembly 'C:\BCN\PowerShell\cleanup-digas\modules\MySql.Data.dll'
#initiates holder for rows
$ParentTableList = [System.Collections.ArrayList]@()
#load configuration for the database
$FileSetting = 'C:\BCN\PowerShell\cleanup-digas\setting.json'
$Setting = Get-Content -Raw -Path $FileSetting | ConvertFrom-Json -Depth 4
# run cleanup script on ready
Write-Host "Start Cleaning"
$Directories = @('files', 'results', 'tables')
$MainPath = "C:\BCN\PowerShell\cleanup-digas"

# cleanup old temporary data
$CleanupIndex = 1
foreach ($Directory in $Directories) {
  $Path = "$MainPath\$Directory"
  if (Test-Path $Path) {
    $Items = Get-ChildItem $Path
    if ($null -ne $Items) {
      Remove-Item $Items -Force -Recurse
    } 
  }

  $CleanUpPercentStatus = [math]::Round($CleanupIndex/$Directories.Count) * 100
  Write-Progress -Activity "Cleanup temporary data" -Status "$Directory - $CleanupIndex/$($Directories.Count)" -PercentComplete $CleanUpPercentStatus
  $CleanupIndex += 1
}

# delete old status log result
if (Test-Path "$MainPath\status.md") {
  Remove-Item "$MainPath\status.md" -Force
}

Write-Host "Finish cleaning"
# mounting file share
if ($IsWindows -eq $true) {
  
}
else {
  # make string connection
  Invoke-Expression "mount_smbfs //$($Setting.Azure.Account)@$($Setting.Azure.Account).file.core.windows.net/$($Setting.Azure.Fileshare.Name) fileshares" | Get-Process
}
$constr = "server=$($Setting.Server);port=$($Setting.$Port);user id=$($Setting.User);password=$($Setting.Password);database=$($Setting.Database);defaultcommandtimeout=30;connectiontimeout=15"
#make connection instance to mysql server
$con = New-Object MySql.Data.MySqlClient.MySqlConnection($constr)
# $con.ConnectionString = $constr
#open/start connection to mysql server
$con.Open()
#make command sql instance for get all parent tables
$cmd = New-Object MySql.Data.MySqlClient.MySqlCommand
Write-Host "Scanning Database"
$cmd.CommandText = "SELECT table_name FROM information_schema.tables WHERE table_schema = '$($Setting.Database)' and table_name NOT REGEXP '_' ORDER by table_name"
$cmd.Connection = $con
#execute query
$parentTables = $cmd.ExecuteReader()
#add data to data holder
# $ScanningDBIndex = 1
While ($parentTables.Read() -eq $true) {
  $ParentTable = New-Object -TypeName psobject
  # Write-Progress -Activity "Mapping Table From Database" -Status "Status $ScanningDBIndex%"

  #mapping data to object
  For ($i = 0; $i -lt $parentTables.FieldCount; $i++) { 
    Add-Member -InputObject $ParentTable -MemberType 'NoteProperty' -Name $parentTables.GetName($i) -Value $parentTables.GetValue($i)
  }

  # filter for some table which doesnt have column FILENAME
  if ($Setting.IgnoreTables.Contains($ParentTable.table_name) -eq $false) {
    if ($ParentTable.table_name -eq 'archivlangzeit') {
      $null = $ParentTableList.Add($ParentTable.table_name)
    }
  }
}
# close connection to current query to clear data reader
$parentTables.close()

# get data from parent table
foreach ($ParentTableName in $ParentTableList) {
  $cmd.CommandText = "SELECT FILENAME, SOFTDEL FROM $ParentTableName";
  $RowsFromTable = [System.Collections.ArrayList]@()
  $Rows = $cmd.ExecuteReader()

  while ($Rows.Read() -eq $true) {
    $Row = New-Object -TypeName psobject

    #mapping data to object
    For ($i = 0; $i -lt $Rows.FieldCount; $i++) {
      Add-Member -InputObject $Row -MemberType 'NoteProperty' -Name $Rows.GetName($i) -Value $Rows.GetValue($i)
    }

    # [System.Collections.ArrayList]@()
    $Row.FILENAME = $Row.FILENAME -replace "(\\\\)?\w+:?\\?\\\w+(\W)?\\?\\(\w+\\)?", ""
    $null = $RowsFromTable.Add($Row)
    # $null = $ChildrenTableList.Add($ParentTable.table_name)
  }

  $Rows.Close()
  $RowsToString = $RowsFromTable | ConvertTo-Json -AsArray
  $null = New-Item -Path "C:\BCN\PowerShell\cleanup-digas\tables\$($ParentTableName).json" -Value $RowsToString -Force
  # Replace some name because has different value with table's name on database
  $DirectoryNameOnStorage = $ParentTableName
  switch ($ParentTableName) {
    'archivfilm' { $DirectoryNameOnStorage = 'archivfilme'; Break; }
    'import' { $DirectoryNameOnStorage = 'ImportMP3'; Break; }
    'korrespondenten' { $DirectoryNameOnStorage = 'Korris'; Break; }
  }
  # Get all files in file share folder
  if ($IsWindows) {
    $childrenFile = Get-ChildItem "$($Setting.DigasPath)\$DirectoryNameOnStorage" | Select-Object -Property Name
  }
  else {
    $childrenFile = Get-ChildItem "C:\BCN\PowerShell\cleanup-digas\fileshares\$DirectoryNameOnStorage" -Name | Select-Object -Property Value | Add-Member -MemberType AliasProperty -Name 'Name' -Value $_.Value -PassThru
  }

  $ChunkName = 0
  $ChunkLimit = 100
  $ChunkCounter = 0
  $ChunkDataHolder = [System.Collections.ArrayList]@()
  $PerLimitTimes = 0

  # #Delete existing json
  # $filesToDelete = Get-ChildItem -Path 'C:\BCN\PowerShell\cleanup-digas\files' | Where-Object { $_.Name -match "^$ParentTableName" }
  # $filesToDelete | Remove-Item

  # create directory
  $null = New-Item -Name $ParentTableName -ItemType 'directory' -Path 'C:\BCN\PowerShell\cleanup-digas\files'

  for ($i = 0; $i -lt $childrenFile.Count; $i++) {
    $null = $ChunkDataHolder.Add($childrenFile[$i].Name)
    $ChunkCounter += 1

    if (($ChunkCounter -eq $ChunkLimit) -or ((($PerLimitTimes * $ChunkLimit) + $ChunkDataHolder.Count) -eq $childrenFile.Count)) {
      $Data = $ChunkDataHolder | ConvertTo-Json -AsArray
      $null = New-Item -Path "C:\BCN\PowerShell\cleanup-digas\files\$ParentTableName\$ChunkName.json" -Value $Data -Force
      
      if ($ChunkCounter -eq $ChunkLimit) {
        $PerLimitTimes += 1
      }

      $ChunkCounter = 0
      $ChunkName += 1
      $ChunkDataHolder.Clear()
    }
  }
}

$null = "|Table Name|Total on DB|Total on Storage|Total Deleted|Total Soft Deleted|Different (DB - Storage)|" | Out-File -NoClobber -FilePath 'C:\BCN\PowerShell\cleanup-digas\status.md' -Encoding Ascii
$null = "|---|---|---|---|---|---|" | Out-File -Append -NoClobber -FilePath 'C:\BCN\PowerShell\cleanup-digas\status.md' -Encoding Ascii

# iterate for filtering/deleting audio files
$ParentTableIndex = 1
foreach ($ParentTableName in $ParentTableList) {
  $ChunkPath = "C:\BCN\PowerShell\cleanup-digas\files\$ParentTableName"

  # if (($ParentTableName -ne 'archivfilm')) {
  $TotalChunk = 0
  $ItemsShouldRemoved = [System.Collections.ArrayList]@()
  $ItemsShouldRemovedSoftDelete = [System.Collections.ArrayList]@()

  $ChunkFiles = Get-ChildItem -Path $ChunkPath
  $FilesInDatabase = [System.Collections.ArrayList]@()
  $FilesInDatabaseSoftDelete = [System.Collections.ArrayList]@()
  
  $tmp = Get-Content "C:\BCN\PowerShell\cleanup-digas\tables\$ParentTableName.json" | ConvertFrom-Json
  
  foreach ($t in $tmp) {
    if ($t.SOFTDEL -eq 1) {
      $ItemsShouldRemovedSoftDelete.Add($t.FILENAME)
    } elseif ($t.SOFTDEL -eq 0) {
      $null = $FilesInDatabase.Add($t.FILENAME)
    }
  }
  
  for ($ChunkIndex = 0; $ChunkIndex -lt $ChunkFiles.Count; $ChunkIndex++) {
    $ChunkFileName = "$ChunkIndex.json"
    $FilePath = "C:\BCN\PowerShell\cleanup-digas\files\$ParentTableName\$ChunkFileName"
  
    $FilesInStorage = [System.Collections.ArrayList]@()
    $FilesInStorage = Get-Content $FilePath | ConvertFrom-Json
  
    $TotalChunk += $FilesInStorage.Count
    # total all chunks
    if ($FilesInDatabase.Count -gt 0) {
      foreach ($item in $FilesInStorage) {
        if (!$FilesInDatabase.Contains($item)) {
          $null = $ItemsShouldRemoved.Add($item)
          # Remove-Item "L:\$ParentTableName\$($item)" -Force
        } elseif ($FilesInDatabaseSoftDelete.Contains($item)) {
          $null = $ItemsShouldRemovedSoftDelete.Add($item)
        }
      }
    }
  
    # $FilesInStorage.Clear()
    $FilePercentage = [math]::Round((($ChunkIndex + 1) / $ChunkFiles.Count) * 100)
    Write-Progress -ParentId 0 -Id 1 -Activity "Scanning" -Status "Progress $FilePercentage%" -PercentComplete $FilePercentage -CurrentOperation $ChunkFileName
  }
  
  $RemoveFiles = $ItemsShouldRemoved | ConvertTo-Json -AsArray
  $RemoveFilesSoftDelete = $ItemsShouldRemovedSoftDelete | ConvertTo-Json -AsArray
  # $existFiles = $ExistingONDatabase | ConvertTo-Json -AsArray
  $null = New-Item -Path "C:\BCN\PowerShell\cleanup-digas\results\$ParentTableName.json" -Value $RemoveFiles -Force
  $null = New-Item -Path "C:\BCN\PowerShell\cleanup-digas\results\$($ParentTableName)_SOFTDEL.json" -Value $RemoveFilesSoftDelete -Force
  # $null = New-Item -Path "C:\BCN\PowerShell\cleanup-digas\results\$ParentTableName\exists.json" -Value $existFiles -Force
  $TotalDiff = $FilesInDatabase.Count - $TotalChunk
  
  $null = "|$ParentTableName|$($FilesInDatabase.Count)|$TotalChunk|$($ItemsShouldRemoved.Count)|$($ItemsShouldRemovedSoftDelete.Count)|$TotalDiff|" | Out-File -NoClobber -FilePath 'C:\BCN\PowerShell\cleanup-digas\status.md' -Append -Encoding Ascii
      
  $ParentTablePercentage = [math]::Round(($ParentTableIndex / $ParentTableList.Count) * 100)
  Write-Progress -Id 0 -Activity "Scanning" -Status "Progress $ParentTablePercentage%" -PercentComplete $ParentTablePercentage -CurrentOperation $ParentTableName
  $ParentTableIndex += 1
  # }
}


# close connection when done with database
$con.close()
# unmount file share
if ($IsWindows -eq $true) {

}
else {
  Invoke-Expression "umount ./fileshares"
}


# in71894002
# }
# catch {
#   $null = New-Item -Path "C:\BCN\PowerShell\cleanup-digas\logs\log.txt" -Value $_.Exception.StackTrace -Force
# }