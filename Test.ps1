$Data = Get-Content -Raw -Path './tables/news.json' | ConvertFrom-Json -Depth 4

# Write-Host $Data

foreach ($d in $Data) {
  # $t = $d.FILENAME -replace "\w{1}:\\?\\\w+\\?\\", ""
  $t = $d.FILENAME -replace "(\\\\)?\w+:?\\?\\\w+(\W)?\\?\\(\w+\\)?", ""
  # $t = $d.FILENAME -replace "\\\\\w+\\\w+$", ""
  # $s = $t -replace "\\", ""
  Write-Host $t
}